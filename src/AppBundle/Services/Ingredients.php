<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 6/8/2019
 * Time: 12:51 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Disease;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class Ingredients
{

    /** @var  Tesseract */
    protected $ocrEngine;

    /** @var  EntityManager */
    protected $entityManager;
    /**
     * @param $barcode string
     */
    public function getIngredientsForBarCode($barcode)
    {
        $data = json_decode(file_get_contents('https://world.openfoodfacts.org/api/v0/product/'.$barcode.'.json'), true);

        if (!isset($data['product']['ingredients_text_ro'])){
            throw new \Exception('Nu am gasit produsul.');
        }
        /** @var Ingredient[] $allIngredients */
        $allIngredients = $this->getEntityManager()->getRepository(Ingredient::class)->findAll();

        $ingredients = [];
        foreach ($allIngredients as $ingredient){
            $ingredients[$ingredient->getCod()] = $ingredient;
        }

        $result = $this->getIngredientsFromText($data['product']['ingredients_text_ro']);

        if (isset($data['product']['additives_original_tags'])) {
            foreach ($data['product']['additives_original_tags'] as $aditive) {
                $aditiveCode = $this->cleanUpAditive($aditive);
                if (isset($ingredients[$aditiveCode])) {
                    $result[$aditiveCode] = $ingredients[$aditiveCode];
                }
            }
        }

        return $result;
    }

    private function cleanUpAditive($aditive){
        $data =explode(':',$aditive);

        return isset($data[1])?trim(strtoupper($data[1])):trim(strtoupper($aditive));
    }

    /**
     * @param $imagePath string
     */
    public function getIngredientsFormImage($imagePath)
    {
        $text = $this->getOcrEngine()->getText($imagePath);

        return $this->getIngredientsFromText(strtoupper($text));
    }

    public function getIngredientsFromText($text)
    {
        setlocale(LC_CTYPE, 'ro_RO');
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        $text = strtoupper($text);
        /** @var Ingredient[] $allIngredients */
        $allIngredients = $this->getEntityManager()->getRepository(Ingredient::class)->findAll();

        $result = [];
        $ingredients = [];
        $regexes = [];
        foreach ($allIngredients as $ingredient){
            $matches = [];
            if (preg_match('/E\s?([0-9]{3,4})/', $ingredient->getCod(), $matches)){
                $regexes[]='\bE\s?'.$matches[1].'[A-Z]?\b';
                $regexes[]='\bE\s?'.strtoupper($ingredient->getDenumire()).'[A-Z]?\b';
            } else {
                $regexes[]='\b'.$ingredient->getCod().'\b';
            }
            $ingredients[$ingredient->getCod()] = $ingredient;
            $ingredients[strtoupper($ingredient->getDenumire())] = $ingredient;
        }
        $regex = '/'.implode($regexes,'|').'/';

        preg_match_all($regex, $text, $matches);
        $ma = [];
        foreach ($matches[0] as $match){
            if (preg_match('/E\s?([0-9]{3,4})/', $match, $ma)){
                $cleanUp = 'E'.$ma[1];
                $result[$ingredients[$cleanUp]->getCod()] = $ingredients[$cleanUp];
            } else {
                $result[$ingredients[$match]->getCod()] = $ingredients[$match];
            }
        }

        return $result;
    }

    /**
     * @param User $user
     * @param Ingredient[] $ingredients
     */
    public function sortIngredients(User $user, $ingredients)
    {
        $result = [];
        foreach ($ingredients as $ingredient){
            $allowed = $ingredient->getAllowedQuantity($user);
            $result[$ingredient->getCod()]= $ingredient;
        }

        return $result;
    }

    /**
     * @param User $user
     * @param Ingredient[] $ingredients
     */
    public function getMinAllowedQuatity(User $user, $ingredients)
    {
        $min = 1000042;
        foreach ($ingredients as $ingredient){
            $allowed = $ingredient->getAllowedQuantity($user);
            if ($allowed<$min){
                $min = $allowed;
            }
        }

        return $min;
    }

    /**
     * @return Tesseract
     */
    public function getOcrEngine()
    {
        return $this->ocrEngine;
    }

    /**
     * @param Tesseract $ocrEngine
     * @return Ingredients
     */
    public function setOcrEngine($ocrEngine)
    {
        $this->ocrEngine = $ocrEngine;

        return $this;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     * @return Ingredients
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }




}