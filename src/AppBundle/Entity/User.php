<?php

/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 02-06-2019
 * Time: 7:02 PM
 */
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=100, nullable=true)
     */
    private $surname;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", length=100, nullable=true)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=100, nullable=true)
     */
    private $sex;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", length=100, nullable=true)
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", length=100, nullable=true)
     */
    private $height;


    /**
     * @var Disease[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Disease")
     * @ORM\JoinTable(name="users_diseases",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="disease_id", referencedColumnName="id")}
     * )
     */
    private $diseases;

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return User
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return User
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function __construct()
    {
        $this->diseases = new ArrayCollection();
    }

    /**
     * Add disease
     *
     * @param \AppBundle\Entity\Disease $disease
     *
     * @return User
     */
    public function addDisease(\AppBundle\Entity\Disease $disease)
    {
        $this->diseases[] = $disease;

        return $this;
    }

    /**
     * Remove disease
     *
     * @param \AppBundle\Entity\Disease $disease
     */
    public function removeDisease(\AppBundle\Entity\Disease $disease)
    {
        $this->diseases->removeElement($disease);
    }

    /**
     * Get diseases
     *
     * @return Disease[]|\Doctrine\Common\Collections\Collection
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return User
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }


    public function getBmi()
    {
        return round ($this->getWeight()/(($this->getHeight()/100)*($this->getHeight()/100)),2);
    }

}
