<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class IngredientAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('cod')
            ->add('denumire')
            ->add('nocivitate')
            ->add('gramaj')
            ->add('gramajProdus')
            ->add('efectAdvers')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('cod')
            ->add('denumire')
            ->add('nocivitate')
            ->add('gramaj')
            ->add('gramajProdus')
            ->add('efectAdvers')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('cod')
            ->add('denumire')
            ->add('nocivitate')
            ->add('gramaj')
            ->add('gramajProdus')
            ->add('efectAdvers')
            ->add('permisions', 'sonata_type_collection',
                array('by_reference' => false, 'label'=>'Permisions'),
                array('edit' => 'inline',
                    'inline' => 'table'
                )
            )
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('cod')
            ->add('denumire')
            ->add('nocivitate')
            ->add('gramaj')
            ->add('gramajProdus')
            ->add('efectAdvers')
        ;
    }
}
