<?php

namespace AppBundle\Controller;

use AppBundle\Services\Ingredients;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template
     */
    public function indexAction(Request $request)
    {

    }

    /**
     * @Route("/scanBarcode", name="scan_barcode")
     * @Template
     */
    public function scanBarcodeAction(Request $request)
    {
        

    }

    /**
     * @Route("/checkBarcode", name="check_barcode")
     * @Template
     */
    public function checkBarcodeAction(Request $request)
    {
        /** @var Ingredients $ingredientsService */
        $ingredientsService = $this->get('ingredients');
        try {
            $ing = $ingredientsService->getIngredientsForBarCode($request->get('code'));
        } catch (\Exception $ex) {
            return $this->redirectToRoute('read_ingredients');
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $sortedIngredientrs=$ingredientsService->sortIngredients($user,$ing);

        $allowedQuantity=$ingredientsService->getMinAllowedQuatity($user,$ing);

        return ['aditives'=>$sortedIngredientrs,'allowedQuantity'=>$allowedQuantity, 'user'=>$user];
    }

    /**
     * @Route("/checkOCR", name="check_ocr")
     * @Template
     */
    public function checkOcrAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('image', FileType::class,['attr'=>['accept'=>'image/*;capture=camera']])
            ->add('save', SubmitType::class, ['label' => 'Trimite'])
            ->setAction($this->generateUrl('check_ocr'))
            ->getForm();
        $form->handleRequest($request);

        if ($request->getMethod()==Request::METHOD_POST){
            /** @var UploadedFile $file */
            $file = $request->files->get('form')['image'];

            $file->move('/var/www/html/GoT/Daniel/web/uploads/', $file->getClientOriginalName());

            $fullPath = '/var/www/html/GoT/Daniel/web/uploads/'.$file->getClientOriginalName();

            /** @var Ingredients $ingredientsService */
            $ingredientsService = $this->get('ingredients');
            $ing = $ingredientsService->getIngredientsFormImage($fullPath);
            //$ing = $ingredientsService->getIngredientsForBarCode($request->get('code'));

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $sortedIngredientrs=$ingredientsService->sortIngredients($user,$ing);

            $allowedQuantity=$ingredientsService->getMinAllowedQuatity($user,$ing);

        }



        return ['aditives'=>$sortedIngredientrs,'allowedQuantity'=>$allowedQuantity, 'user'=>$user];
    }

    /**
     * @Route("/readIngredients", name="read_ingredients")
     * @Template
     */
    public function readIngredientsAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('image', FileType::class,['attr'=>['accept'=>'image/*;capture=camera']])
            ->add('save', SubmitType::class, ['label' => 'Trimite'])
            ->setAction($this->generateUrl('check_ocr'))
            ->getForm();
        $form->handleRequest($request);

        if ($request->getMethod()==Request::METHOD_POST){
            /** @var UploadedFile $file */
            $file = $request->files->get('form')['image'];

            $file->move('/var/www/html/GoT/Daniel/web/uploads/', $file->getClientOriginalName());

            $fullPath = '/var/www/html/GoT/Daniel/web/uploads/'.$file->getClientOriginalName();

        }

        return ['form'=>$form->createView()];
    }
}
