<?php
namespace AppBundle\Services;
use thiagoalessio\TesseractOCR\Command;
use thiagoalessio\TesseractOCR\TesseractOCR;

/**
 * Created by PhpStorm.
 * User: Tavi
 * Date: 04-Jun-19
 * Time: 22:47 PM
 */
class Tesseract
{

    public function getText($path)
    {
        $command = new Command();
        $command->options[]="-l ron";
        $instance = new TesseractOCR($path);
        $text = $instance->run();
        setlocale(LC_CTYPE, 'ro_RO');
        return iconv('UTF-8', 'ASCII//TRANSLIT',$text);
    }

}