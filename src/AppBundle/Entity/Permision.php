<?php
/**
 * Created by PhpStorm.
 * User: Tavi
 * Date: 08-Jun-19
 * Time: 18:40 PM
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Permision
 *
 * @ORM\Table(name="permisions")
 * @ORM\Entity
 */
class Permision
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *  @var integer
     *  @ORM\Column(name="permis", type="string", length=30, nullable=false)
     */
    private $permis;

    /**
     * @var Ingredient
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ingredient", inversedBy="permisions")
     * @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $ingredient;


    /**
     * @var Disease
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Disease")
     * @ORM\JoinColumn(name="disease_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $disease;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Permision
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * @param mixed $permis
     * @return Permision
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;
        return $this;
    }

    /**
     * @return Disease
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * @param Disease $disease
     * @return Permision
     */
    public function setDisease($disease)
    {
        $this->disease = $disease;
        return $this;
    }

    /**
     * @return Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param Ingredient $ingredient
     * @return Permision
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;

        return $this;
    }




}