<?php
/**
 * Created by PhpStorm.
 * User: Andreea
 * Date: 04-06-2019
 * Time: 2:45 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class UserController extends Controller
{
    /**
     * @param $userId
     * @Route ("/editProfile/{userId}", name="editProfile")
     */
    public function editProfileAction($userId )
    {
        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->find($userId);
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));



        return $this->redirect('../admin/app/user/'.$userId.'/edit');
    }


    /**
     *
     * @Route ("/TyFprofile", name="TyFprofile")
     * @Template
     */
    public function TyFprofileAction()
    {
        /* @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if ($this->container->get('security.token_storage')->getToken()->getUser() != 'anon.') {
            /* @var User $userLogIn */
            $userLogIn = $this->container->get('security.token_storage')->getToken()->getUser();

            return['userLogIn'=>$userLogIn, 'bmi'=>$userLogIn->getBmi()];

        }
        else {
            return ($this->redirectToRoute('login'));
        }

    }


}