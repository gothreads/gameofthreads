<?php
/**
 * Created by PhpStorm.
 * User: Tavi
 * Date: 08-Jun-19
 * Time: 12:34 PM
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="ingrediente")
 * @ORM\Entity
 */
class Ingredient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="cod", type="string", length=6, nullable=false)
     */
    private $cod;

    /**
     * @var string
     * @ORM\Column(name="denumire", type="string", length=30, nullable=false)
     */
    private $denumire;

    /**
     * @var string
     * @ORM\Column(name="nocivitate", type="string", length=30, nullable=false)
     */
    private $nocivitate;

    /**
     * @var float
     * @ORM\Column(name="gramaj", type="float", nullable=false)
     */
    private $gramaj;

    /**
     * @var float
     * @ORM\Column(name="gramaj_produs", type="float", nullable=false)
     */
    private $gramajProdus;

    /**
     * @var string
     * @ORM\Column(name="efect_advers", type="string", length=300, nullable=false)
     */
    private $efectAdvers;

    /**
    * @var Permision[]
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\Permision", mappedBy="ingredient", cascade={"persist"})
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id")
    * })
    */
    private $permisions;



    function __toString()
    {
        return $this->getCod();
    }


    /**
     * @return string
     */
    public function getCod(): string
    {
        return $this->cod;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @param string $cod
     * @return Ingredient
     */
    public function setCod(string $cod): Ingredient
    {
        $this->cod = $cod;
        return $this;
    }

    /**
     * @return string
     */
    public function getDenumire(): string
    {
        return $this->denumire;
    }

    /**
     * @param string $denumire
     * @return Ingredient
     */
    public function setDenumire(string $denumire): Ingredient
    {
        $this->denumire = $denumire;
        return $this;
    }

    /**
     * @return string
     */
    public function getNocivitate(): string
    {
        return $this->nocivitate;
    }

    /**
     * @param string $nocivitate
     * @return Ingredient
     */
    public function setNocivitate(string $nocivitate): Ingredient
    {
        $this->nocivitate = $nocivitate;
        return $this;
    }

    /**
     * @return float
     */
    public function getGramaj(): float
    {
        return $this->gramaj;
    }

    /**
     * @param float $gramaj
     * @return Ingredient
     */
    public function setGramaj(float $gramaj): Ingredient
    {
        $this->gramaj = $gramaj;
        return $this;
    }

    /**
     * @return float
     */
    public function getGramajProdus(): float
    {
        return $this->gramajProdus;
    }

    /**
     * @param float $gramajProdus
     * @return Ingredient
     */
    public function setGramajProdus(float $gramajProdus): Ingredient
    {
        $this->gramajProdus = $gramajProdus;
        return $this;
    }

    /**
     * @return string
     */
    public function getEfectAdvers(): string
    {
        return $this->efectAdvers;
    }

    /**
     * @param string $efectAdvers
     * @return Ingredient
     */
    public function setEfectAdvers(string $efectAdvers): Ingredient
    {
        $this->efectAdvers = $efectAdvers;
        return $this;
    }




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->permisions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add permision
     *
     * @param \AppBundle\Entity\Permision $permision
     *
     * @return Ingredient
     */
    public function addPermision(\AppBundle\Entity\Permision $permision)
    {
        $this->permisions[] = $permision;
        $permision->setIngredient($this);

        return $this;
    }

    /**
     * Remove permision
     *
     * @param \AppBundle\Entity\Permision $permision
     */
    public function removePermision(\AppBundle\Entity\Permision $permision)
    {
        $this->permisions->removeElement($permision);
    }

    /**
     * Get permisions
     *
     * @return Permision[]|\Doctrine\Common\Collections\Collection
     */
    public function getPermisions()
    {
        return $this->permisions;
    }

    /**
     * @param User $user
     * @return float|null
     */
    public function getAllowedQuantity(User $user)
    {
        $diseases = [];
        foreach ($user->getDiseases() as $disease){
            $diseases[$disease->getId()]=$disease;
        }

        foreach ($this->getPermisions() as $permision) {
            if (isset($diseases[$permision->getDisease()->getId()]) && $permision->getPermis() == 'Nu'){
                return 0;
            }
        }
        if ($this->getGramajProdus()==0 || $this->getGramaj()){
            return 1000042;
        }
        return ($user->getWeight()*$this->getGramaj())/$this->getGramajProdus()*1000;
    }
}
